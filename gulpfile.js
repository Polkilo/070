var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    del = require('del'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    cssshrink = require('gulp-cssshrink'),
    cp = require('child_process'),
    changed = require('gulp-changed'),
    imagemin = require('gulp-imagemin'),
    size = require('gulp-size'),
    ghPages = require('gulp-gh-pages');


gulp.task('styles', function() {
  gulp.src('./src/common/sass/**/*.scss')
    //.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    //.pipe(sourcemaps.write())
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest('src/common/css'))
    .pipe(gulp.dest('public/common/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    //.pipe(cssshrink())
    .pipe(gulp.dest('src/common/css'))
    .pipe(gulp.dest('public/common/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('noPrefix', function (){
    return gulp.src('./src/common/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('.', {
            includeContent: false,
            sourceRoot: '../../../src/common/scss'
        }))
        .pipe(gulp.dest('./src/common/css'));
});

gulp.task('scripts', function() {
  return gulp.src(['./src/common/js/**/*.js', '!./src/common/js/**/*.min.js'])
    .pipe(gulp.dest('public/common/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('public/common/js'))
    .pipe(browserSync.reload({stream:true}));
});
gulp.task('script2', function() {
  return gulp.src(['./src/common/js/**/*.min.js'])
    .pipe(gulp.dest('public/common/js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/common/js'))
    .pipe(browserSync.reload({stream:true}));
});

// Optimizes the images that exists
gulp.task('images', function () {
  return gulp.src('src/common/images/**')
    .pipe(changed('public/common/images'))
    .pipe(imagemin({
      // Lossless conversion to progressive JPGs
      progressive: true,
      // Interlace GIFs for progressive rendering
      interlaced: true
    }))
    .pipe(gulp.dest('public/common/images'))
    .pipe(size({title: 'images'}));
});

gulp.task('html', function() {
  gulp.src('./src/**/*.html')
    .pipe(gulp.dest('public/'))
});

gulp.task('browser-sync', ['styles', 'scripts', 'script2'], function() {
  browserSync({
    server: {
      baseDir: "./public/",
      injectChanges: true // this is new
    }
  });
});

gulp.task('deploy', function() {
  return gulp.src('./public/**/*')
    .pipe(ghPages());
});

gulp.task('watch', function() {
  // Watch .html files
  gulp.watch('src/**/*.html', ['html', browserSync.reload]);
  gulp.watch("public/**/*.html").on('change', browserSync.reload);
  // Watch .sass files
  gulp.watch('src/common/sass/**/*.scss', ['styles', browserSync.reload]);
  gulp.watch('src/common/css/**/*.css', ['noPrefix', browserSync.reload]);
  gulp.watch('src/common/css/**/*.css.map', ['noPrefix', browserSync.reload]);
  // Watch .js files
  gulp.watch('src/common/js/*.js', ['scripts', browserSync.reload]);
  gulp.watch('src/common/js/*.js', ['script2', browserSync.reload]);
  // Watch image files
  gulp.watch('src/common/images/**/*', ['images', browserSync.reload]);
});

gulp.task('default', function() {
    gulp.start('styles', 'noPrefix', 'scripts', 'script2', 'images', 'html', 'browser-sync', 'watch');
});