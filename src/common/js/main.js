$(window).resize(function() {
	conversionImages();
});

$(document).ready(initPage);
function initPage(){
	conversionImages();
	mobileMenu();
	anchor();

	$(window).on("load",function(){
		$(".scroll").mCustomScrollbar({
			axis:"y",
			theme:"light",
			scrollButtons: false
		});
	});

	var heightEl  = 0;
	$(window).scrollTop() > heightEl ? $('#header').addClass('scrolled') : $('#header').removeClass('scrolled');
	$(window).scroll(function(){
		$(window).scrollTop() > heightEl ? $('#header').addClass('scrolled') : $('#header').removeClass('scrolled');
	});
}

function anchor(){
	$(".anchor").click(function(e){
		var href = $(this).attr("href"),
		offsetTop = href === "#" ? 0 : $(href).offset().top - $('#header').outerHeight();
		$('body').removeClass('menu-opened');
		$("html, body").stop().animate({
			scrollTop: offsetTop
		}, 1000);
		e.preventDefault();
	});
};

function conversionImages(){
	$('.bg').each(function() {
		$(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')').find('> img').hide();
	});
	$('.visual-top').each(function() {
		if (window.innerWidth >= 768) {
			$(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')').find('> img').hide();
		} else if (window.innerWidth <= 768) {
			$(this).css('background-image', 'url(' + $(this).find('> img.mb-bg').attr('src') + ')').find('> img').hide();
		}
	});
}

function mobileMenu(){
	$('<a href="#" class="open-menu"><span></span>Open Menu</a>').prependTo('#header .container');
	$('<span class="fader"/>').appendTo('#wrapper');
	$('.open-menu, .close').click(function(){
		$('body').toggleClass('menu-opened');
		return false;
	});
	$('.main-nav .btn.btn-default').click(function(){
		$('body').toggleClass('menu-opened');
		return false;
	});
	$('.fader').click(function(){
		$('body').removeClass('menu-opened');
	});
	$('#header .nav li a').click(function(){
		$('body').removeClass('menu-opened');
	});
	
	$(document).on('touchmove', function(event) {
		if ($('body').hasClass('menu-opened') && $(window).width() < 992) {
			if ($('#main-nav').has(event.target).length) {
			return true;
			} else {
				event.preventDefault();
			}
		}
	});
}

var OnScroll_a = function() {

	$(document).ready(function(){
		setTimeout(OnScroll,300);
	});
	if(window.location.hash){
		window.hash = window.location.hash.substr(1);
		window.location.hash = '';
	}
	function OnScroll() {
		if (window.hash) {
			var scrollPos = $('#'+window.hash).offset().top -= document.querySelector('#header').offsetHeight;
			$("html, body").animate({ scrollTop: scrollPos }, 1000);
		}
	};

}();